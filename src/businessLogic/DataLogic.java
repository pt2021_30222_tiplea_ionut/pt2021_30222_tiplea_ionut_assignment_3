package businessLogic;

import model.Client;
import model.OrderProduct;
import model.Orderr;
import model.Product;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.*;

public class DataLogic {
    private int id = 0;
    private ArrayList<Client> clients = new ArrayList<>(0);
    private ArrayList<Product> products = new ArrayList<>(0);
    private Map<Orderr,ArrayList<OrderProduct>> orders = new HashMap<>(0);
    public DataLogic() {
        populateClients();
        populateOrders();
        populateProducts();
    }
    public void populateClients() {
        this.clients = (ArrayList<Client>) Client.getClientList();
    }
    public void populateProducts() {
        this.products = (ArrayList<Product>) Product.getProductList();
    }
    public void populateOrders() {
        List<Orderr> list = Orderr.getOrderrs();
        for(Orderr o : list){
            orders.put(o, (ArrayList<OrderProduct>) OrderProduct.getOrderProductsById(o.getOrderId()));
            id ++;
        }
    }
    public ArrayList<Client> getClients() {
        return (ArrayList<Client>) clients.clone();
    }
    public ArrayList<Product> getProducts() {
        return (ArrayList<Product>) products.clone();
    }
    public Map<Orderr,ArrayList<OrderProduct>> getOrders(){
        return orders;
    }
    public static JTable getObjectTable(ArrayList<Object> list) {
        JTable tb;
        Field[] columns = list.get(0).getClass().getDeclaredFields();
        List<String> columnNames = new ArrayList<>(0);
        for (Field f : columns) {
            columnNames.add(f.getName());
            //System.out.print(f.getName() +",");
        }
        System.out.println();
        List<String[]> data = new ArrayList<>(0);
        data.add((String[]) columnNames.toArray(new String[columnNames.size()]));
        try {
            for (Object c : list) {
                Field[] fList = c.getClass().getDeclaredFields();
                StringBuilder line = new StringBuilder();
                for (Field f : fList) {
                    f.setAccessible(true);
                    line.append(f.get(c)).append(" ");
                }
                data.add(line.toString().split(" "));
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        tb = new JTable((String[][]) data.toArray(new String[data.size()][]), (String[]) columnNames.toArray(new String[columnNames.size()]));
        return tb;
    }
    public void addClient(String s) {
        Client theClient = Client.fromString(s);
        if(theClient != null) {
            theClient.insert();
            clients.add(theClient);
        }
    }
    public void addProduct(String s) {
        Product theProduct = Product.fromString(s);
        if(theProduct != null) {
            theProduct.insert();
            products.add(theProduct);
        }
    }
    public void changeClient(String oldClient, String newClient){
        Client old = Client.fromString(oldClient);
        Client nw = Client.fromString(newClient);
        if(old != null && nw != null){
            clients.remove(old);
            clients.add(nw);
            nw.update();
        }
    }
    public void changeProduct(String oldProduct, String newProduct){
        Product old = Product.fromString(oldProduct);
        Product nw = Product.fromString(newProduct);
        if(old != null && nw != null){
            products.remove(old);
            products.add(nw);
            nw.update();
        }
    }
    public void deleteClient(String s){
        Client c = Client.fromString(s);
        if(c != null){
            clients.remove(c);
            c.delete();
        }
    }
    public void deleteProduct(String s){
        Product p = Product.fromString(s);
        if(p != null){
            products.remove(p);
            p.delete();
        }
    }
    public boolean createOrder(String client, HashMap<Integer,Integer> productList){
        Client c = Client.fromString(client);
        Orderr order = new Orderr(++id,c.getId());
        ArrayList<OrderProduct> op = new ArrayList<>(0);
        for(Map.Entry<Integer,Integer> product : productList.entrySet()){
            int pid = product.getKey();
            int quantity = product.getValue();
            Product nw = null;
            Product old = null;
            for(Product p : products){
                if(p.getId() == pid){
                    if(p.getStock() >= quantity){
                        op.add(new OrderProduct(order.getOrderId(),pid,quantity));
                        nw = new Product(pid,p.getStock()-quantity,p.getPrice());
                        old = p;
                    }
                }
            }
            if(nw != null) {
                nw.update();
                products.remove(old);
                products.add(nw);
            }
        }
        orders.put(order,op);
        order.insert();
        if(op.size() == 0){
            return false;
        }
        for(OrderProduct o : op){
            o.insert();
        }
        return true;
    }
}
