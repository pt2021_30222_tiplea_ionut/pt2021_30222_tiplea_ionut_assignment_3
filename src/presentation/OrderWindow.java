package presentation;

import businessLogic.DataLogic;
import model.Client;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class OrderWindow implements ActionListener {
    DataLogic data;
    private static JFrame f;
    private static JPanel p = new JPanel();
    private static JTextField client = new JTextField(10);
    private static JTextArea products = new JTextArea(5 , 5);
    private static JTable clientTable,productTable;
    private static JButton showAllClientsButton, showAllProductsButton,makeOrderButton;
    public OrderWindow(DataLogic data){
        this.data = data;
        f = new JFrame();
        products.setBackground(new Color(100,200,100));
        showAllClientsButton = new JButton("Show Clients");
        showAllClientsButton.addActionListener(this);
        showAllProductsButton = new JButton("Show Products");
        showAllProductsButton.addActionListener(this);
        makeOrderButton = new JButton("Make Order");
        makeOrderButton.addActionListener(this);
        p.add(showAllClientsButton);
        p.add(showAllProductsButton);
        p.add(client);
        p.add(products);
        p.add(makeOrderButton);
        p.setVisible(true);
        f.add(p);
        p.setLayout(new FlowLayout());
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {

            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == showAllClientsButton){
            if(clientTable != null)
                p.remove(clientTable);
            ArrayList<Client> arr = data.getClients();
            ArrayList<Object> arrCpy = new ArrayList<>(arr);
            clientTable = DataLogic.getObjectTable(arrCpy);
            p.add(clientTable);
            p.updateUI();
        }
        if(e.getSource() == showAllProductsButton){
            if(productTable != null)
                p.remove(productTable);
            ArrayList<Product> arr = data.getProducts();
            ArrayList<Object> arrCpy = new ArrayList<>(arr);
            productTable = DataLogic.getObjectTable(arrCpy);
            p.add(productTable);
            p.updateUI();
        }
        if(e.getSource() == makeOrderButton){
            HashMap<Integer,Integer> map = new HashMap<>(0);
            String[] prod = products.getText().split("\n");
            for(String s : prod){
                String[] s2 = s.split(" ");
                try{
                    map.put(Integer.parseInt(s2[0]),Integer.parseInt(s2[1]));
                }
                catch(Exception ex){
                    JOptionPane.showMessageDialog(f, "Wrong Product Format");
                }
            }
            //System.out.println(Arrays.toString(prod));
            if(map.size() > 0) {
                boolean b = data.createOrder(client.getText(), map);
            }
            else
                JOptionPane.showMessageDialog(f, "Wrong order format/Out of stock");
        }
    }
}
