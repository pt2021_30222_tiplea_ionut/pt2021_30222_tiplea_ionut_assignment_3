package presentation;

import businessLogic.DataLogic;
import model.Client;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientOperationGUI implements ActionListener {
    DataLogic data;
    private static JFrame f;
    private static JPanel p = new JPanel();
    private static JTable tb = new JTable();
    //private static JScrollPane scrollPane = new JScrollPane(tb);
    private static JButton showAllButton,addClientButton,changeClientButton,deleteClientButton;
    private static JTextField addClient = new JTextField(10);
    private static JTextField oldClient = new JTextField(10);
    private static JTextField newClient = new JTextField(10);
    public ClientOperationGUI(DataLogic data){
        this.data = data;
        f = new JFrame();
        tb = new JTable(3,3);
        showAllButton = new JButton("Show All Clients");
        addClientButton = new JButton("Add Client");
        showAllButton.addActionListener(this);
        changeClientButton = new JButton("Change Client");
        changeClientButton.addActionListener(this);
        addClientButton.addActionListener(this);
        deleteClientButton = new JButton("Delete Client");
        deleteClientButton.addActionListener(this);
        //p.add(tb);
        //p.add(scrollPane);
        p.add(showAllButton);
        p.add(addClient);
        p.add(addClientButton);
        p.add(deleteClientButton);
        p.add(oldClient);
        p.add(newClient);
        p.add(changeClientButton);
        p.setVisible(true);
        f.add(p);
        p.setLayout(new FlowLayout());
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {

            }
        });
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == showAllButton){
            p.remove(tb);
            ArrayList<Client> arr = data.getClients();
            ArrayList<Object> arrCpy = new ArrayList<>(arr);
            tb = DataLogic.getObjectTable(arrCpy);
            p.add(tb);
            p.updateUI();
            //SwingUtilities.updateComponentTreeUI(f);
            //System.out.println("BEen here");
        }
        if(e.getSource() == addClientButton){
            data.addClient(addClient.getText());
        }
        if(e.getSource() == changeClientButton){
            data.changeClient(oldClient.getText(),newClient.getText());
        }
        if(e.getSource() == deleteClientButton){
            data.deleteClient(addClient.getText());
        }
    }
}
