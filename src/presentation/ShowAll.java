package presentation;

import businessLogic.DataLogic;
import model.Client;
import model.Product;

import java.sql.SQLException;
import java.util.ArrayList;

public class ShowAll {
    public static void main(String[] args) {
        DataLogic d = new DataLogic();
        ArrayList<Client> arr = d.getClients();
        ArrayList<Product> arr2 = d.getProducts();
        ClientOperationGUI c = new ClientOperationGUI(d);
        ProductOperationGUI p = new ProductOperationGUI(d);
        OrderWindow o = new OrderWindow(d);
    }
}
