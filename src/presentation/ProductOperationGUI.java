package presentation;

import businessLogic.DataLogic;
import model.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ProductOperationGUI implements ActionListener {
    DataLogic data;
    private static JFrame f;
    private static JPanel p = new JPanel();
    private static JTable tb;
    private static JTextField addProduct = new JTextField(10);
    private static JTextField oldProduct = new JTextField(10);
    private static JTextField newProduct = new JTextField(10);
    private static JButton showAllButton,addProductButton,changeProductButton,deleteProductButton;
    public ProductOperationGUI(DataLogic data){
        this.data = data;
        f = new JFrame("Product Window");
        tb = new JTable(3,3);
        showAllButton = new JButton("Show All Products");
        showAllButton.addActionListener(this);
        addProductButton = new JButton("Add Product");
        addProductButton.addActionListener(this);
        changeProductButton = new JButton("Change Product");
        changeProductButton.addActionListener(this);
        deleteProductButton = new JButton("Delete Product");
        deleteProductButton.addActionListener(this);
        //p.add(tb);
        p.add(showAllButton);
        p.add(addProduct);
        p.add(addProductButton);
        p.add(deleteProductButton);
        p.add(oldProduct);
        p.add(newProduct);
        p.add(changeProductButton);
        p.setVisible(true);
        f.add(p);
        p.setLayout(new FlowLayout());
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == showAllButton){
            p.remove(tb);
            ArrayList<Product> arr = data.getProducts();
            ArrayList<Object> arrCpy = new ArrayList<>(arr);
            tb = DataLogic.getObjectTable(arrCpy);
            p.add(tb);
            p.updateUI();
            //SwingUtilities.updateComponentTreeUI(f);
            //System.out.println("BEen here");
        }
        if(e.getSource() == addProductButton){
            data.addProduct(addProduct.getText());
        }
        if(e.getSource() == changeProductButton){
            data.changeProduct(oldProduct.getText(),newProduct.getText());
        }
        if(e.getSource() == deleteProductButton){
            data.deleteProduct(addProduct.getText());
        }
    }
}
