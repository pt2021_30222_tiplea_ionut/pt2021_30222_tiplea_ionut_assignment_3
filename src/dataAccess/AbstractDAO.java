package dataAccess;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.FileWriter;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import dataAccess.ConnectionFactory;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 * @Source http://www.java-blog.com/mapping-javaobjects-database-reflection-generics
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private String createSelectQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + type.getDeclaredFields()[0].getName()+ " =?");
        return sb.toString();
    }
    private String createSelectAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }
     private String createInsertQuery(List<Object> values){
        StringBuilder sb = new StringBuilder();
        sb.append("Insert ");
        sb.append("INTO ");
        sb.append(type.getSimpleName());
        sb.append(" Values (");
        for(Object o : values){
            sb.append("?,");
        }
        sb.deleteCharAt(sb.length() -1);
        sb.append("); ");
        //System.out.println(sb.toString());
        return sb.toString();
    }
    private String createUpdateQuery(Field[] list, T t){
        StringBuilder sb = new StringBuilder();
        sb.append("Update ")
        .append(type.getSimpleName())
        .append(" Set ");
        try {
            for (Field f : list) {
                f.setAccessible(true);
                if (f.get(t) instanceof String)
                    sb.append(f.getName()).append(" = ").append(f.get(t)).append(",");
                else
                    sb.append(f.getName()).append(" = ").append(f.get(t)).append(",");
            }
        }
        catch(IllegalAccessException e){
            e.printStackTrace();
        }
        sb.deleteCharAt(sb.length() -1);
        sb.append(" Where ");
        sb.append(list[0].getName());
        sb.append(" = ");
        try {
            sb.append(list[0].get(t));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        sb.append(";");
        return sb.toString();
    }
    private String createDeleteQuery(Field f,T t){
        StringBuilder sb = new StringBuilder();
        sb.append("Delete FROM ").append(type.getSimpleName());
        sb.append(" WHERE ");
        try {
            f.setAccessible(true);
            sb.append(f.getName()).append(" = ").append(f.get(t)).append(";");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    public List<T> findAllById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T)ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return list;
    }

    public boolean insert(T t){
        List<Object> list = new ArrayList<>(0);
        Field[] fields = t.getClass().getDeclaredFields();
        for(Field f : fields){
            f.setAccessible(true);
            try {
                if(f.get(t) != null)
                    list.add(f.get(t));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return false;
            }
        }
        if(list.size() != 0){
            Connection connection = null;
            PreparedStatement statement = null;
            ResultSet resultSet = null;
            String query = createInsertQuery(list);
            try {
                connection = ConnectionFactory.getConnection();
                statement = connection.prepareStatement(query);
                int i = 1;
                for(Object o : list) {
                    statement.setObject(i ++ ,o);
                }
                statement.execute();
            } catch (SQLException e) {
                LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
                //System.out.println(query);
                return false;
            } finally {
                ConnectionFactory.close(resultSet);
                ConnectionFactory.close(statement);
                ConnectionFactory.close(connection);
            }
        }
        else
            return false;
        return true;
    }
    public boolean insertList(List<T> list){
        for(T t : list){
            if(!insert(t))
                return false;
        }
        return true;
    }
    public boolean update(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createUpdateQuery(t.getClass().getDeclaredFields(),t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return true;
    }
    public boolean delete(T t){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createDeleteQuery(t.getClass().getDeclaredFields()[0],t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.execute();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
            return false;
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return true;
    }
}
