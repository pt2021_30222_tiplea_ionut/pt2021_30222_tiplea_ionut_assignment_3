package model;

import dataAccess.OrderProductDAO;
import dataAccess.OrderrDAO;
import dataAccess.ProductDAO;

import java.util.ArrayList;
import java.util.List;

public class Orderr {
    private int orderId;
    private int clientId;
    //private static int id = 0;
    /**
     * Implicit constructor used for implicit declarations
     */
    public Orderr(){
        orderId = 0;
        clientId = 0;
    }

    /**
     *
     * @param clientId
     * Actual constructor, used to declare full fledged Orders
     */
    public Orderr(int orderId,int clientId){
        this.orderId = orderId;
        this.clientId = clientId;
    }
    public int getOrderId(){
        return orderId;
    }
    public int getClientId(){
        return  clientId;
    }
    public void insert(){
        OrderrDAO op = new OrderrDAO();
        op.insert(this);
    }
    public static List<Orderr> getOrderrs(){
        OrderrDAO o = new OrderrDAO();
        return o.findAll();
    }
    public void setOrderId(int id){
        this.orderId = id;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }
}
