package model;

import dataAccess.ClientDAO;
import dataAccess.ProductDAO;

import java.util.List;

public class Product {
    private int id;
    private int stock;
    private float price;//in euros
    public Product(){
        id = 0;
        stock = 0;
        price = 0;
    }
    public Product(int id, int stock, float price){
        this.id = id;
        this.stock = stock;
        this.price = price;
    }
    public int getId(){
        return id;
    }
    public int getStock(){
        return stock;
    }
    public float getPrice(){
        return price;
    }
    public void setId(int id){
        this.id =  id;
    }
    public void setStock(int stock){
        this.stock = stock;
    }
    public void setPrice(float price){
        this.price = price;
    }
    public static List<Product> getProductList(){
        ProductDAO p = new ProductDAO();
        return p.findAll();
    }
    public void insert(){
        ProductDAO p = new ProductDAO();
        p.insert(this);
    }
    public static Product fromString(String s){
        String[] split = s.split(",");
        if (split.length == 3) {
            try {
                int c1 = Integer.parseInt(split[0]);
                int c2 = Integer.parseInt(split[1]);
                int c3 = Integer.parseInt(split[2]);
                Product myProduct = new Product(c1, c2, c3);
                return myProduct;
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return null;
    }
    public void update(){
        ProductDAO p = new ProductDAO();
        p.update(this);
    }
    public void delete(){
        ProductDAO p = new ProductDAO();
        p.delete(this);
    }
    public boolean equals(Object other){
        if(other instanceof Product){
            Product c = (Product) other;
            return this.id == c.id && this.stock == c.stock && this.price == c.price;
        }
        else
        {
            return false;
        }
    }
}
