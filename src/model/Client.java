package model;

import dataAccess.ClientDAO;

import java.util.List;

public class Client  {
    private int id;
    private String name;
    private String contact;

    /**
     * Implicit Constructor
     */
    public Client(){
        id = 0;
        name = "";
        contact = "";
    }

    /**
     * Actual constructor for creating a full fledged object
     * @param id
     * @param name
     * @param contact
     */
    public Client(int id, String name, String contact){
        this.id = id;
        this.name = name;
        this.contact = contact;
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public String getContact() {
        return this.contact;
    }
    public static List<Client> getClientList(){
        ClientDAO c = new ClientDAO();
        return c.findAll();
    }
    public void insert(){
        ClientDAO c = new ClientDAO();
        c.insert(this);
    }
    public static Client fromString(String s){
        String[] split = s.split(",");
        if (split.length == 3) {
            try {
                int c = Integer.parseInt(split[0]);
                Client myClient = new Client(c, split[1], split[2]);
                return myClient;
            } catch (NumberFormatException e) {
                ;
                return null;
            }
        }
        return null;
    }
    @Override
    public boolean equals(Object other){
        if(other instanceof Client){
            Client c = (Client)other;
            return this.id == c.id && this.contact.equals(c.contact) && this.name.equals(c.name);
        }
        else
        {
            return false;
        }
    }
    public void update(){
        ClientDAO c = new ClientDAO();
        c.update(this);
    }
    public void delete(){
        ClientDAO c = new ClientDAO();
        c.delete(this);
    }
    public void setId(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setContact(String contact){
        this.contact = contact;
    }
}
