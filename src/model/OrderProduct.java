package model;

import dataAccess.OrderProductDAO;

import java.util.List;

public class OrderProduct {
    private int orderId;
    private int productId;
    private int quantity;
    public OrderProduct(int orderId,int productId,int quantity) {
        this.productId = productId;
        this.orderId = orderId;
        this.quantity = quantity;
    }
    public OrderProduct(){
        orderId = 0;
        productId = 0;
        quantity = 0;
    }
    public int getOrderId() {
        return orderId;
    }

    public int getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }
    public void insert(){
        OrderProductDAO op = new OrderProductDAO();
        op.insert(this);
    }
    public static List<OrderProduct> getOrderProductsById(int id){
        OrderProductDAO op = new OrderProductDAO();
        return op.findAllById(id);
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
